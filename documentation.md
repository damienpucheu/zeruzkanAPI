# Documentation avancée de l'API Zeruzkan

---

**Rechercher des vols**

Vous pouvez rechercher des vols selon différents critères.

Pour les requêtes contenant des paramètres dans l'URL, ces paramètres sont notés ":parameter".

Certaines requêtes nécessitent des paramètres contenus dans le corps de la requête HTTP.

----

***Récupérer tous les vols existants***
Utilisez cette URI pour récupérer tous les vols existants dans le système.

* **URL**

  /api/flight/all

* **Méthodes disponibles:**

    GET

* **Méthode GET:**

    GET /api/flight/all

* **Paramètres d'URL**

Aucun paramètre d'URL n'est requis, ni optionnel.

* **Paramètres du corps de requête**

Aucun paramètre de requête n'est requis, ni optionnel.

* **Succès**

  * **Code:** 200 <br/>
    **Contenu:** Réponse JSON contenant tous les vols existants dans la base de données.
    `{
    "_id": "58f64839254164166b66642f",
    "idCompany": "2B",
    "numberFlight": 413,
    "airportDeparture": "AER",
    "avaibilityJ": 29,
    "avaibilityY": 34,
    "avaibilityM": 5,
    "airportArrival": "KZN",
    "hourDeparture": 1200,
    "hourArrival": 1324,
    "dateFlight": "07MAY",
    "priceJ": 170.45,
    "priceY": 136.6,
    "priceM": 540,
    "taxJ": 63.27,
    "taxY": 82.23,
    "taxM": 73.43,
    "__v": 0,
    "updated_at": "2017-04-18T17:09:13.387Z",
    "reservation": []
  },`

* **Erreur**

Aucune erreur n'est possible.

----

***Récupérer les vols selon une date, un aéroport de départ et un aéroport de destination***

* **URL**

  /api/flight/all/:date/:origin/:destination/

* **Méthodes disponibles:**

    GET

* **Méthode GET:**

  GET /api/flight/all/:date/:origin/:destination/

* **Paramètres d'URL**

   **Requis**

   `date=[String]` au format JJ/MMM (04FEB)

   `origin=[String]` au format AAA ('CDG' pour l'aéroport Charles De Gaulle)

   `destination=[String]` au même format qu'origin

   **Optionnel**

Aucun

* **Paramètres du corps de requête**

Aucune paramètre de requête.

* **Succès**

  * **Code:** 200 <br/>
  **Contenu:** Le serveur retourne une réponse en JSON contenant tous les vols partant de "origin" à "destination" à la date "date"
  `{
  "_id": "58f64839254164166b66642f",
  "idCompany": "2B",
  "numberFlight": 413,
  "airportDeparture": "AER",
  "avaibilityJ": 29,
  "avaibilityY": 34,
  "avaibilityM": 5,
  "airportArrival": "KZN",
  "hourDeparture": 1200,
  "hourArrival": 1324,
  "dateFlight": "07MAY",
  "priceJ": 170.45,
  "priceY": 136.6,
  "priceM": 540,
  "taxJ": 63.27,
  "taxY": 82.23,
  "taxM": 73.43,
  "__v": 0,
  "updated_at": "2017-04-18T17:09:13.387Z",
  "reservation": []
},`

* **Erreur**

Si un est null

----

***Récupérer les vols selon une date, un aéroport de départ, un aéroport de destination et une heure***

* **URL**

  /api/flight/all/:date/:origin/:destination/:hour

* **Méthodes disponibles:**

    GET

* **Méthode GET:**

  GET /api/flight/all/:date/:origin/:destination/:hour

  * **Paramètres d'URL**

     **Requis**

     `date=[String]04FEB` au format JJ/MMM

     `origin=[String]CDG` au format AAA

     `destination=[String]ORY` au format AAA

     `hour=[integer]1540` au format HHMM

   **Optionnel**

Aucun

* **Paramètres du corps de requête**

Aucune paramètre de requête.

* **Succès**

  * **Code:** 200 <br/>
  **Contenu:** Le serveur retourne une réponse en JSON contenant tous les vols partant de "origin" à "destination" à la date "date" au moins une heure avant "hour".
  `{
  "_id": "58f64839254164166b66642f",
  "idCompany": "2B",
  "numberFlight": 413,
  "airportDeparture": "AER",
  "avaibilityJ": 29,
  "avaibilityY": 34,
  "avaibilityM": 5,
  "airportArrival": "KZN",
  "hourDeparture": 1200,
  "hourArrival": 1324,
  "dateFlight": "07MAY",
  "priceJ": 170.45,
  "priceY": 136.6,
  "priceM": 540,
  "taxJ": 63.27,
  "taxY": 82.23,
  "taxM": 73.43,
  "__v": 0,
  "updated_at": "2017-04-18T17:09:13.387Z",
  "reservation": []
},`

* **Erreur**

Si un est null

----

***Récupérer tous les vols d'une compagnie***

* **URL**

  /api/flight/bycompany/:companyName

* **Méthodes disponibles:**

    GET

* **Méthode GET:**

  GET /api/flight/bycompany/:companyName

* **Paramètres d'URL**

   **Requis**

   `companyName=[String]2B` au format AA

   **Optionnel**

Aucun

* **Paramètres du corps de requête**

Aucune paramètre de requête.

* **Succès**

  * **Code:** 200 <br/>
  **Contenu:** Le serveur retourne une réponse en JSON contenant tous les vols dont la compagnie est "companyName"
  `{
  "_id": "58f64839254164166b66642f",
  "idCompany": "2B",
  "numberFlight": 413,
  "airportDeparture": "AER",
  "avaibilityJ": 29,
  "avaibilityY": 34,
  "avaibilityM": 5,
  "airportArrival": "KZN",
  "hourDeparture": 1200,
  "hourArrival": 1324,
  "dateFlight": "07MAY",
  "priceJ": 170.45,
  "priceY": 136.6,
  "priceM": 540,
  "taxJ": 63.27,
  "taxY": 82.23,
  "taxM": 73.43,
  "__v": 0,
  "updated_at": "2017-04-18T17:09:13.387Z",
  "reservation": []
},`

* **Erreur**

Si pas de vols

----

***Récupérer tous les vols d'une date***

* **URL**

  /api/flight/bydate/:date

* **Méthodes disponibles:**

    GET

* **Méthode GET:**

  GET /api/flight/bydate/:date

* **Paramètres d'URL**

   **Requis**

   `date=[String]04FEB` au format JJMMM

   **Optionnel**

Aucun

* **Paramètres du corps de requête**

Aucune paramètre de requête.

* **Succès**

  * **Code:** 200 <br/>
  **Contenu:** Le serveur retourne une réponse en JSON contenant tous les vols dont la date est "date"
  `{
  "_id": "58f64839254164166b66642f",
  "idCompany": "2B",
  "numberFlight": 413,
  "airportDeparture": "AER",
  "avaibilityJ": 29,
  "avaibilityY": 34,
  "avaibilityM": 5,
  "airportArrival": "KZN",
  "hourDeparture": 1200,
  "hourArrival": 1324,
  "dateFlight": "07MAY",
  "priceJ": 170.45,
  "priceY": 136.6,
  "priceM": 540,
  "taxJ": 63.27,
  "taxY": 82.23,
  "taxM": 73.43,
  "__v": 0,
  "updated_at": "2017-04-18T17:09:13.387Z",
  "reservation": []
},`

* **Erreur**

Si pas de vols

----

***Effectuer une réservation***

Une réservation ne peut avoir lieu qu'après avoir effectué une recherche.
Si aucune recherche n'a été effectuée, le serveur enverra une erreur.

* **URL**

  /api/flight/reservation

* **Méthodes disponibles:**

    POST, PUT

-----

* **Méthode POST:**

  POST /api/flight/reservation

* **Paramètres d'URL**

   **Requis**

Aucun

   **Optionnel**

Aucun

* **Paramètres du corps de requête**

   **Requis**

  `numberOfSeats=[int]1` Nombre de sièges à associer à la réservation

  `seatClass=[String]Y` Valeur limitée à J, Y ou M. Ce sont les classes de sièges disponibles.

  `displayedLineNumber=[int]4` Numéro de ligne du vol lors de l'affichage

   **Optionnel**

Aucun

* **Succès**

  * **Code:** 200 <br/>
  **Contenu:** Le serveur retourne une réponse en JSON contenant le vol pour lequel la réservation a été créée
  `{
      "_id": "58f64839254164166b666430",
      "idCompany": "2B",
      "numberFlight": 320,
      "airportDeparture": "ASF",
      "avaibilityJ": 23,
      "avaibilityY": 6,
      "avaibilityM": 0,
      "airportArrival": "KZN",
      "hourDeparture": 2110,
      "hourArrival": 2235,
      "dateFlight": "07MAY",
      "priceJ": 180.45,
      "priceY": 136.6,
      "priceM": 796,
      "taxJ": 84.03,
      "taxY": 54.37,
      "taxM": 45.18,
      "__v": 0,
      "updated_at": "2017-04-18T17:09:13.399Z",
      "reservation": [
      {
          "_id": "B1CIWc2fZ",
          "updated_at": "2017-06-12T22:10:29.563Z",
          "seat": []
      }
      ]
    }`

* **Erreur**

----

***Création de sièges***

Créer des sièges au sein d'une réservation

    * **URL**

      /api/flight/reservation/:reservationID

    * **Méthodes disponibles:**

        GET, POST, PATCH, PUT

-----

    * **Méthode GET:**

      GET /api/flight/reservation/:reservationID

    * **Paramètres d'URL**

       **Requis**

    `reservationID=[String]xB4f2h8_F47` ID de la réservation créée lors de la requête de création de réservation. Disponible dans la réponse de création de la réservation, ou dans les cookies.

       **Optionnel**

    Aucun

    * **Paramètres du corps de requête**

       **Requis**

    Aucun

       **Optionnel**

    Aucun

    * **Succès**

      * **Code:** 200 <br/>
      **Contenu:** Le serveur retourne une réponse en JSON contenant la réservation demandée.
      `{
          "_id": "B1CIWc2fZ",
          "updated_at": "2017-06-12T22:10:29.563Z",
          "seat": []
        }`

    * **Erreur**

-----

    * **Méthode POST:**

      POST /api/flight/reservation/:reservationID

    * **Paramètres d'URL**

   **Requis**

   `reservationID=[String]xB4f2h8_F47` ID de la réservation créée lors de la requête de création de réservation. Disponible dans la réponse de création de la réservation, ou dans les cookies.

   **Optionnel**

   Aucun

* **Paramètres du corps de requête**

   **Requis**

   `numberOfSeats[int]=2` Nombre de sièges à créer dans la réservation
   `seatClass[String]=Y` Classe des sièges à créer. Nécessairement J, Y ou M
   `displayedLineNumber[int]=5` Ligne à laquelle le vol est apparu lors de l'affichage

   **Optionnel**

Aucun

* **Succès**

  * **Code:** 201 <br/>
  **Contenu:** Le serveur retourne une réponse en JSON contenant la réservation pour laquelle on a créé les sièges.
  `{
      "_id": "B1CIWc2fZ",
      "updated_at": "2017-06-12T22:10:29.563Z",
      "seat": []
    }`

* **Erreur**

-----

    * **Méthode PATCH:**

    Permet d'assigner des passagers à des sièges d'une réservation

      PATCH /api/flight/reservation/:reservationID

* **Paramètres d'URL**

    **Requis**

    `reservationID=[String]xB4f2h8_F47` ID de la réservation créée lors de la requête de création de réservation. Disponible dans la réponse de création de la réservation, ou dans les cookies.

    **Optionnel**

Aucun

    * **Paramètres du corps de requête**

    **Requis**

    `firstName[String]=Andoni` Prénom du passager à associer au siège
    `lastName[String]=Pucheu` Nom du passager à associer au siège
    `gender[String]=Homme` Genre du passager à associer au siège
    `seatClass[String]=Y` Classe du siège à associer au passager. Nécessairement J, Y ou M.

    **Optionnel**

Aucun

    * **Succès**

    * **Code:** 201 <br/>
    **Contenu:** Le serveur retourne une réponse en JSON contenant le vol pour laquelle on a réservé le siège.
    `{
        "_id": "B1CIWc2fZ",
        "updated_at": "2017-06-12T22:10:29.563Z",
        "seat": []
    }`

* **Erreur**

-----

    * **Méthode PUT:**

Permet de valider une réservation et de la transformer en ticket.

    PUT /api/flight/reservation/:reservationID

    * **Paramètres d'URL**

    **Requis**

`reservationID=[String]xB4f2h8_F47` ID de la réservation créée lors de la requête de création de réservation. Disponible dans la réponse de création de la réservation, ou dans les cookies.

    **Optionnel**

Aucun

    * **Paramètres du corps de requête**

    **Requis**

Aucun

    **Optionnel**

Aucun

    * **Succès**

    * **Code:** 200 <br/>
    **Contenu:** Le serveur retourne une réponse en JSON contenant le vol pour lequel on a validé la réservation.
    `{
        "_id": "B1CIWc2fZ",
        "updated_at": "2017-06-12T22:10:29.563Z",
        "seat": []
    }`

* **Erreur**

----

***Récupération et suppression des tickets***

Permet de récupérer un ticket ou de le supprimer.

    * **URL**

      /api/flight/ticket/:ticketID

    * **Méthodes disponibles:**

        GET, DELETE

    * **Méthode GET:**

Permet de récupérer un ticket.

    GET /api/flight/ticket/:ticketID

    * **Paramètres d'URL**

    **Requis**

`ticketID=[String]xB4f2h8_F47` ID du ticket, correspond à l'ID de la réservation validée. Disponible dans la réponse de création du ticket, ou dans les cookies.

    **Optionnel**

Aucun

    * **Paramètres du corps de requête**

    **Requis**

Aucun

    **Optionnel**

Aucun

    * **Succès**

    * **Code:** 200 <br/>
    **Contenu:** Le serveur retourne une réponse en JSON contenant le vol pour lequel on a demandé le ticket.
    `{
        "_id": "B1CIWc2fZ",
        "updated_at": "2017-06-12T22:10:29.563Z",
        "seat": []
    }`

* **Erreur**

-----

* **Méthode DELETE:**

Permet de supprimer un ticket.

DELETE /api/flight/ticket/:ticketID

* **Paramètres d'URL**

**Requis**

`ticketID=[String]xB4f2h8_F47` ID du ticket, correspond à l'ID de la réservation validée. Disponible dans la réponse de création du ticket, ou dans les cookies.

**Optionnel**

Aucun

* **Paramètres du corps de requête**

**Requis**

Aucun

**Optionnel**

Aucun

* **Succès**

* **Code:** 200 <br/>
**Contenu:** Le serveur retourne une réponse texte indiquant que le ticket a été supprimé.
`Le ticket a bien été supprimé`

* **Erreur**

---

* **Error Response:**

  <_Most endpoints will have many ways they can fail. From unauthorized access, to wrongful parameters etc. All of those should be liste d here. It might seem repetitive, but it helps prevent assumptions from being made where they should be._>

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `{ error : "Log in" }`

  OR

  * **Code:** 422 UNPROCESSABLE ENTRY <br />
    **Content:** `{ error : "Email Invalid" }`

* **Sample Call:**

  <_Just a sample call to your endpoint in a runnable format ($.ajax call or a curl request) - this makes life easier and more predictable._>

* **Notes:**

  <_This is where all uncertainties, commentary, discussion etc. can go. I recommend timestamping and identifying oneself when leaving comments here._>
