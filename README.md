# Zeruzkan
Engineering school project made by Larzabal Andoni (AndoniLarz) and Pucheu Damien (Puch)

# Goal
The goal is to provide a flight reservation system using a REST API.
We were able to use almost any language so we chose to focus on NodeJs in order to learn REST and Node together.

# Where is the name coming from ?
Since this is a flight reservation system, we thought of SkyScanner, a famous flight reservation system.
In the Basque language, Sky is said Zeru (pronounced "sérou" with the 'r' being rolled, like in Spanish).
And the "Zkan" part comes from, and is pronounced like, Scan.

If you join both parts, you get Zeruzkan, the Basque SkyScanner ! :)

# How do I install this shit bro ?

Well it's pretty simple :

## 1 - Install NodeJs locally

Go to : https://nodejs.org/en/download/ or, if like us you prefer to use the package-manager of your OS, go to https://nodejs.org/en/download/package-manager/

Follow the documentation, according to your OS.

## 2 - Check that node and npm are installed

If you installed everything succesfully you should be able to do :

    node --version

And get something like

    v6.10.2

Also, make sure npm is installed by doing this :

    npm -v

You'll get the version of npm (node's packet manager) and the satisfaction of having a clean install !

## 3 - Get the project

Now, you should clone this project in a directory of your choice (the one you use for your own projects for example).

In order to do it, type this in your terminal :

    git clone https://gitlab.com/ad4ltrophy/zeruzkanAPI.git

It should download the source code in a new directory.

## 4 - Install the packets required by the project

This is why npm is installed, you only have to type this and let the magic happen :

    npm install

We're almost done !

## 5 - Launch the server

Like above, npm is there for us :

    npm start

The server should start, connect to the database and begin the listening.

## 6 - Launch the client

Open another terminal and type :

    node client.js

## 6 bis - Realize the client is not functional and use Postman in order to test

During the development of this project, we used Postman in order to simulate a client's HTTP requests.
It's very easy to use and also manages cookies.

To use it, you can go to https://www.getpostman.com/ and download it or use it as an extension to your browser (which is compulsory to manage cookies).

After you've downloaded it, make sure the Interceptor module is active.

Now, import the Zeruzkan.postman_collection.json to your Postman.
This collection is a bundle of all the requests you need to test the API, ready to use.
Open the different requests and customize your parameters (URL and/or body) and click on SEND !


## 7 - Feedback et explications

Pendant ce projet, nous avons découvert le langage NodeJS. De ce fait, nous sommes conscients que notre code n'est pas parfait, ni totalement fonctionnel.

NodeJS ayant la particularité d'être asynchrone, nous avons eu de fortes difficultés à maîtriser le comportement de l'application. De ce fait, nous avons dupliqué de nombreuses lignes de code plutôt que de les factoriser dans des fonctions pour la seule et unique raison qu'un appel de fonction est lui aussi asynchrone et que nous ne comprenions pas comment gérer cette absence de synchronisation pour attendre la réponse et pouvoir continuer normalement l'exécution du code.

D'autre part, le serveur nous ayant pris un certain temps à développer (en parallèle avec le projet du module de Systèmes d'Information), nous avons eu peu de temps restant pour le client (en témoignent les commits).
Il n'est donc pas totalement fonctionnel.
Cependant, nous avons utilisé l'extension Postman tout au long du projet pour simuler le client, et elle nous a apporté entière satisfaction. C'est pour cela que nous vous mettons à disposition le bundle des requêtes, prêtes à utiliser, pour tester le serveur.
Vous n'avez qu'à modifier les paramètre d'URL et de body de la requête et à cliquer sur Send.

Enfin, la documentation présente sur le wiki n'est pas complète non plus. Nous avons tenté de faire au mieux avec le temps restant.

Au moment de faire le bilan de ce projet nous estimons que malgré les difficultés rencontrées et le fait que le client et la documentation ne soient pas terminés, nous avons appris beaucoup de choses.
L'univers des technologies et la communauté gravitant autour de NodeJS forment un monde à part qui était et reste d'un très grand intérêt pour nous deux.
Parmi les compétences gagnées ou abordées, nous donnerions en particulier les Promise, MongoDB et Mongoose, Express et la gestion des bases de données en NoSQL.

Evidemment, nous avons énromément appris sur les API REST et sur leur philosophie.
Nous avons même pensé à en développer une dans un projet personnel que nous développons ensemble en PHP avec le framework Symfony.
